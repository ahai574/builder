<?php
/*
 * @Description: 构建器：方式一，路由方式直接返回json；方式二，hash请求，查找路由，请求路由并返回json
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2021-04-23 10:08:25
 * @LastEditTime: 2021-07-13 11:33:42
 * @LastEditors: 阿海
 */
namespace builder;

use builder\ApiResult;

class Builder{

    /**
     * 统一输出结果类
     * @param class
     */
    static protected $apiResult;

    /**
     * 初始化
     */
    public function __construct()
    {
         self::$apiResult = new ApiResult();
    }

    /**
     * 切换builder的入口
     * @param string $type 构建器名称，'Form', 'Table','Tree','Echart' == 构建对应的json格式
     * @return mixed
     * @throws Exception
     */
    public static function store($type = '')
    {
        if ($type == '') {
            //未指定构建器名称
            return self::$apiResult->echoJson(70001);
        } else {
            $type = ucfirst($type);
        }

        // 构造器类路径
        $class = '\\builder\\driver\\' . $type;
        if (!class_exists($class)) {
            //构建器不存在
            return self::$apiResult->echoJson(70002,['class_name'=>$class]);
        }

        return new $class;
    }

 }