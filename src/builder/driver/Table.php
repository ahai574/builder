<?php
/*
 * @Description: 驱动器：构建from表单的json数据
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2021-04-23 10:08:25
 * @LastEditTime: 2021-07-13 11:35:14
 * @LastEditors: 阿海
 */
namespace builder\driver;
use builder\Builder;


 class Table extends Builder{
     
    /**
     * 列间距 可选 “像素值” 数组
     * @var array
     */
    private $_space = [0, 1, 3, 5, 8, 10, 12, 15, 18, 20, 22, 28, 30];
     
    /**
     * @var array 模板参数变量
     */
    protected $_vars = [
       'store'=>'soultable',//标识 小写字母 === 作为渲染表格的标识 soultable==》可拖拽表格，treegrid=>树形表格
       'debug'=>false, //检测开启了那些事件，以及是否规范，打印信息 console.log
       'base_url'=>'',//基础页面路由
       'page_title'=>'',//页面内容区上面的标题
       'page_tips'=>'',//页面上搜索区域下面的声明文字【常用于注释给管理员】 可以使用html代码
       'is_layer_page_tips'=>false,//页面上搜索区域下面的声明文字 以什么方式显示f false:以文本方式 true:使用layer的弹出层方式显示
       'hide_checkbox' => false,// 是否隐藏第一列复选框
       'hide_radio'=>true,//是否隐藏单选框
       'columns'=>[],//数据表的列选项数组
       'right_buttons'=>[],//额外拓展的按钮组

       'drag'=>false,//列拖拽
       'row_drag'=>false,//是否开启行拖拽，需要设置 拖拽之后数据 更新的接口
       'row_drag_url'=>'',//拖拽之后数据的更新的接口
       'row_drag_field'=>'row_drag',//更新排序的字段

       'total_row'=>false,//表格底部开启 合计行 作为统计
       'auto_column_width'=>false,//自动行宽，根据内容的长度设置长度 此时字段中设置的 width 将失效

       'is_show_add'=>true,//是否显示新增按钮
       'top_add_show_qrcode'=>false,//表格顶部按钮区域--新增按钮---以二维码方式显示或者跳转url
       'top_add_url'=>'javascript:;',//表格顶部按钮区域---新增数据的url
       'top_add_title'=>'新增',//表格顶部按钮区域---新增数据的url 的 标题
       'is_hash_add'=>true,//hash跳转

       'top_update_cache_url'=>'',//树形表格更新路由缓存的url

       'is_show_right_column'=>true,//是否显示右侧操作列
       'right_column_title'=>'操作',//右侧操作列的标题

       'is_show_delete'=>true,//是否显示删除按钮 【包含选中删除和单个删除按钮】
       'top_delete_url'=>'javascript:;',//表格顶部按钮区域---删除数据的url
       'top_delete_title'=>'删除选中',//表格顶部按钮区域---删除按钮的 标题
       'right_delete_url'=>'javascript:;',//表格右侧按钮区域---删除数据的url
       'right_delete_title'=>'删除',//表格右侧按钮区域---删除按钮的 标题

       'is_show_edit'=>true,//是否显示编辑按钮
       'right_edit_url'=>'javascript:;',//表格右侧按钮区域---编辑数据的url
       'right_edit_title'=>'编辑',//表格右侧按钮区域---编辑数据的url 的 标题
       'is_hash_edit'=>true,//是否 hash|跳转
       'right_edit_url_param_field'=>'id',//可携带参数1对应字段
       'right_edit_url_param_extra_field'=>'',//可携带参数2对应字段
       'right_edit_url_param_field_value'=>'',//可携带参数1对应字段 的固定值
       'right_edit_url_param_extra_field_value'=>'',//可携带参数2对应字段  的固定值

       'search_columns'=>[],  //搜索区域的字段
       'search_url' => '',   // 搜索数据url -- 默认是搜索url
       'table_data_url' => '',   // 表格数据url -- 数据返回格式 就是tp5.1 分页paginate的数据即可

       'is_show_export_all_button'=>false,//是否出现导出全部数据的按钮
       'excel_file_name'=>'',//如果没有设置，则采用页面标题作为文件名，否则使用当前时间
       'export_excel_url'  =>  '', //导出excel 不填默认使用前端excel.js(不能导出图片)导出  否则使用框架直接导出

       'update_field_url'=> '',//更新表格内置的编辑事件的接口
    ];
  
     /**
     * 初始化
     */
    public function __construct()
    {
         parent::__construct();
         $this->_vars['base_url'] = url('cms/Index/index','',true,true);
         $this->_vars['top_add_url'] = url(request()->module()."/".request()->controller().'/add');
         $this->_vars['update_field_url'] = url(request()->module()."/".request()->controller().'/updateField');
         $this->_vars['top_delete_url'] =  url(request()->module()."/".request()->controller().'/delete');
         $this->_vars['right_delete_url'] = url(request()->module()."/".request()->controller().'/delete');
         $this->_vars['right_edit_url'] = url(request()->module()."/".request()->controller().'/edit');
         $this->_vars['top_update_cache_url'] = url(request()->module()."/".request()->controller().'/updateCacheRule');
    }
    
    /**
     * 处理placeholder
     * @param $title
     * @param $placeholder
     * @return array
     */
    protected function makePlaceholder($title, $placeholder = [])
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matche)) {
            $title = $matche[1];
            array_push($placeholder, $matche[2]);
            //并列多个placeholder时，
            if (preg_match('/(.*)\[:(.*)\]/', $matche[1])) {
                return $this->makePlaceholder($matche[1], $placeholder);
            }
        }
        return ['title' => $title, 'placeholder' => array_reverse($placeholder)];
    }

    /**
     * 设置表格的渲染类型：soulTable==》可拖拽表格，treeTable=>树形表格
     * @param string $store
     * @return this
     */
    public function setStore($store = 'soultable')
    {
         $this->_vars['store'] = strtolower($store);
         return $this;
    }

    /**
     * 设置渲染的基础页面路由：url('cms/Index/index')
     * @param string $url
     * @return this
     */
    public function setBaseUrl($url = '')
    {
         empty($url) ? $this->_vars['base_url'] : $url;
         $this->_vars['base_url'] = strtolower($url);
         return $this;
    }

     /**
     * 设置是否开启复选框 
     * @param boolean $boolean
     * @return this
     */
    public function setHideCheckbox($boolean = true)
    {
         $this->_vars['hide_checkbox'] = $boolean;
         return $this;
    }

    /**
     * 设置是否开启单选框 
     * @param boolean $boolean
     * @return this
     */
    public function setHideRadio($boolean = true)
    {
         $this->_vars['hide_radio'] = $boolean;
         return $this;
    }

    /**
     * 检测开启了那些事件，以及是否规范，打印信息 console.log
     * @param boolean $boolean
     * @return this
     */
    public function setDebug($boolean = true)
    {
         $this->_vars['debug'] = $boolean;
         return $this;
    }

    /**
     * 是否开启行拖拽 并设置拖拽之后的更新接口
     * @param string $url 拖拽之后的更新接口 如果没有设置对应接口则行拖拽事件不生效
     * @param boolean $boolean
     * @param string $field 拖拽之后 对应更新的字段
     * @return this
     */
    public function setRowDrag($url = '',$boolean = true,$field='row_drag')
    {
         $this->_vars['row_drag_url'] = $url;
         $this->_vars['row_drag'] = $boolean;
         $this->_vars['row_drag_field'] = $field;
         return $this;
    }

    /**
     * 是否开启合计行
     * @param boolean $boolean
     * @return this
     */
    public function setTotalRow($boolean = true)
    {
         $this->_vars['total_row'] = $boolean;
         return $this;
    }

    /**
     * 是否开启列拖拽
     * @param boolean $boolean
     * @return this
     */
    public function setDrag($boolean = true)
    {
         $this->_vars['drag'] = $boolean;
         return $this;
    }

    /**
     * 表格上右侧列
     * @param string $title
     * @param boolean $boolean 是否显示操作区域
     * @return this
     */
    public function setRightColumn($title = '操作', $boolean = true)
    {
         $this->_vars['right_column_title'] = $title;
         $this->_vars['is_show_right_column'] = $boolean;
         return $this;
    }
    
    /**
     * 页面内容区上面的标题
     * @param string $title
     * @return this
     */
    public function setPageTitle($title = '')
    {
         $this->_vars['page_title'] = $title;
         return $this;
    }

    /**
     * 页面上搜索区域下面的声明文字【常用于注释给管理员】 可以使用html代码
     * @param string $content
     * @return this
     */
    public function setPageTips($content = '',$isLayer = false)
    {
         $this->_vars['page_tips'] = $content;
         $this->_vars['is_layer_page_tips'] = $isLayer;
         return $this;
    }

    /**
     * 更新表格内置的编辑事件的接口
     * @param string $url 链接
     * @return this
     */
    public function setUpdateFieldUrl($url = '')
    {
        $this->_vars['update_field_url'] = trim($url);
        return $this;
    }

    /**
     * 设置请求数据的url
     * @param string $url 链接
     * @return this
     */
    public function setDataUrl($url = '')
    {
        $this->_vars['table_data_url'] = trim($url);
        return $this;
    }

    /**
     * 设置 excel表格导出的url---
     * @param string $url 导出excel的路由地址【php_excel】
     * @param string $fileName 导出excel文件的文件名
     * @param boolean $is_show_export_all_button 是否显示导出所有数据的按钮
     * @return this
     */
    public function setExcelUrl($url = '', $fileName='', $is_show_export_all_button = true)
    {
        $this->_vars['export_excel_url'] = trim($url);
        $this->_vars['is_show_export_all_button'] = (boolean)$is_show_export_all_button;
        $this->_vars['excel_file_name'] =  !empty($fileName) ? mb_convert_encoding($fileName, 'UTF-8', 'UTF-8,GBK,GB2312,BIG5') : (!empty($this->_vars['page_title'])?$this->_vars['page_title']:date("Y_m_d_H_i_s")."xlsx");
        return $this;
    }

    /**
     * 设置搜索参数
     * @param array $fields 参与搜索的字段
     *
     * [['title','标题[:搜索标题]','text'],['create_time','起始时间','datetime'],['sort','分类[:查找分类]','select',['1'=>'分类1',
     * '2'=>'分类2']]]
     *
     * @param string $url 提交地址
     * @return this
     */
    public function setSearch($fields, $url = '')
    {
          try {
               if (!empty($fields) && is_array($fields)) {

                    foreach ($fields as $key => $val) {
                         $name = isset($fields[$key][0]) ? $fields[$key][0] : 'title';
                         $title = isset($fields[$key][1]) ? $fields[$key][1] : '标题';
                         $type = isset($fields[$key][2]) ? $fields[$key][2] : 'text';
                         $value = isset($fields[$key][3]) ? $fields[$key][3] : '';
                         $options = isset($fields[$key][4]) ? $fields[$key][4] : [];
                         $arr = $this->makePlaceholder($title);
                         $layout = [4,6,6,12];//初始化布局
                         // 判断是否有布局参数
                         if (strpos($name, ':')) {
                              list($name, $layout) = explode(':', $name);
                              $layout = explode('|', $layout);
                         }
                         $this->_vars['search_columns'][$key] = [
                              'name' => $name,
                              'type' => $type,
                              'title' => $arr['title'],
                              'options' => $options,
                              'value' => $value,
                              'placeholder' => isset($arr['placeholder'][0]) ? $arr['placeholder'][0] : '',
                              'layout' => 'layui-col-xs-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0) . ' layui-col-sm-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0). ' layui-col-md-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0). ' layui-col-lg-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0) . ' layui-col-space' . (isset($layout[4]) ? ($layout[4] == '' ? $this->_space[0] : $layout[4]) : $this->_space[0]) . ' layui-col-lg' . (isset($layout[3]) ? ($layout[3] == '' ? $layout[0] : $layout[3]) : $layout[0]) . ' layui-col-md' . (isset($layout[2]) ? ($layout[2] == '' ? $layout[0] : $layout[2]) : $layout[0]) . ' layui-col-sm' . (isset($layout[1]) ? ($layout[1] == '' ? $layout[0] : $layout[1]) : $layout[0]) . ' layui-col-xs' . $layout[0],
                         ];
                    }

                    $this->_vars['search_url'] = !empty($url) ? $url : $this->_vars['table_data_url'];
               }
               return $this;
          }catch (Exception $e) {
               throw new Exception($fields . '构建-搜索区域-出错！');
          }
    }

    /**
     * 格式化数据
     */
    private function setSearchValue(){
          foreach ($this->_vars['search_columns'] as $key=>$item) {
               if(empty($this->_vars['search_columns'][$key]['value'])){
                    break;
               }
               switch ($item['type']) {
                    case 'year':
                         if (isset($this->_vars['search_columns'][$item['type']])) {
                              $this->_vars['search_columns'][$key]['value'] = $this->_vars['search_columns'][$item['type']];
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y", time());
                         }
                         //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                         if (stripos(trim($this->_vars['search_columns'][$key]['value']), ":") || stripos(trim($this->_vars['search_columns'][$key]['value']), "-")) {
                              $this->_vars['search_columns'][$key]['value'] = date("Y", strtotime($this->_vars['search_columns'][$key]['value']));
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = date("Y", (int) $this->_vars['search_columns'][$key]['value']);
                         }
                         break;
                    case 'month':
                         if (isset($this->_vars['search_columns'][$item['type']])) {
                              $this->_vars['search_columns'][$key]['value'] = $this->_vars['search_columns'][$item['type']];
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m", time());
                         }
                         //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                         if (stripos(trim($this->_vars['search_columns'][$key]['value']), ":") || stripos(trim($this->_vars['search_columns'][$key]['value']), "-")) {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m", strtotime($this->_vars['search_columns'][$key]['value']));
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m", (int) $this->_vars['search_columns'][$key]['value']);
                         }
                         break;
                    case 'date':
                         if (isset($this->_vars['search_columns'][$item['type']])) {
                              $this->_vars['search_columns'][$key]['value'] = $this->_vars['search_columns'][$item['type']];
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m-d", time());
                         }
                         //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                         if (stripos(trim($this->_vars['search_columns'][$key]['value']), ":") || stripos(trim($this->_vars['search_columns'][$key]['value']), "-")) {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m-d", strtotime($this->_vars['search_columns'][$key]['value']));
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m-d", (int) $this->_vars['search_columns'][$key]['value']);
                         }
                         break;
                    case 'time':
                         if (isset($this->_vars['search_columns'][$item['type']])) {
                              $this->_vars['search_columns'][$key]['value'] = $this->_vars['search_columns'][$item['type']];
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = isset($item['value']) && !empty($item['value']) ? $item['value'] : '00:00:00';
                         }
                         if (!preg_match("/^[0-9]{2}(:){1}[0-9]{2}(:){1}[0-9]{2}$/", $item['value']))
                              $this->_vars['search_columns'][$key]['value'] = "00:00:00";
                         break;
                    case 'datetime':
                         if (isset($this->_vars['search_columns'][$item['type']])) {
                              $this->_vars['search_columns'][$key]['value'] = $this->_vars['search_columns'][$item['type']];
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m-d H:i:s", time());
                         }
                         //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                         if (stripos(trim($this->_vars['search_columns'][$key]['value']), ":") || stripos(trim($this->_vars['search_columns'][$key]['value']), "-")) {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m-d H:i:s", strtotime($this->_vars['search_columns'][$key]['value']));
                         } else {
                              $this->_vars['search_columns'][$key]['value'] = date("Y-m-d H:i:s", (int) $this->_vars['search_columns'][$key]['value']);
                         }
                         break;
                    default:
                         break;
               }
          }
 }

    /**
     * 定制顶部添加按钮
     * 目前一种是 页面跳转 一种是二维码扫码
     * @param boolean $isQrcode  展示二维码方式 或者 跳转url
     * @param array $params ['is_show'=>true, 'type'=>'url|qrcode','url'=>'','']
     * @return this
     */
    public function setTopAddButton($params = [] ,$isQrcode = false)
    {
         $this->_vars['is_show_add'] = isset($params['is_show']) ? (boolean)$params['is_show'] : true; //默认显示
         $this->_vars['is_hash_add'] = isset($params['is_hash']) ? (boolean)$params['is_hash'] : true; //是否hash
         $this->_vars['top_add_show_qrcode'] = $isQrcode;//默认是跳转链接
         $this->_vars['top_add_url'] = isset($params['url']) ? $params['url'] : $this->_vars['top_add_url'];//默认读取 __construct 设置的url
         $this->_vars['top_add_title'] = isset($params['title']) ? $params['title'] : '新增';//标题
         return $this;
    }

    /**
     * 定制右侧添加按钮
     * @param boolean $type  是否显示编辑按钮
     * @param array $params ['is_show'=>true, 'type'=>'url|qrcode','url'=>'','']
     * @return this
     */
    public function setRightEditButton($params = [], $type = true)
    {
          //由于携带参数，所以需要验证是否是通过hash跳转==有参数时，才会需要考虑这个hash
          $this->_vars['is_hash_edit'] = isset($params['is_hash'])?$params['is_hash']:$this->_vars['is_hash_edit'];
          $this->_vars['right_edit_url_param_field'] = isset($params['url_param_field'])?$params['url_param_field']:$this->_vars['right_edit_url_param_field'];
          $this->_vars['right_edit_url_param_extra_field'] = isset($params['url_param_extra_field'])?$params['url_param_extra_field']:$this->_vars['right_edit_url_param_extra_field'];
          $this->_vars['right_edit_url_param_field_value'] = isset($params['url_param_field_value'])?$params['url_param_field_value']:$this->_vars['right_edit_url_param_field_value'];
          $this->_vars['right_edit_url_param_extra_field_value'] = isset($params['url_param_extra_field_value'])?$params['url_param_extra_field_value']:$this->_vars['right_edit_url_param_extra_field_value'];
          $this->_vars['is_show_edit'] = isset($params['is_show']) ? (boolean)$params['is_show'] : $type; //默认显示
          $this->_vars['right_edit_url'] = isset($params['url']) ? $params['url'] : $this->_vars['right_edit_url'];//默认读取 __construct 设置的url
          $this->_vars['right_edit_title'] = isset($params['title']) ? $params['title'] : '编辑';//标题
          // dump( $this->_vars);die;
          return $this;
    }

    /**
     * 定制右侧删除按钮
     * @param boolean $type  是否显示删除按钮
     * @param array $params ['is_show'=>true, 'url'=>'', 'title'=>'删除']
     * @return this
     */
    public function setTopDeleteButton($params = [] ,$type = true)
    {
         $this->_vars['is_show_delete'] = isset($params['is_show']) ? (boolean)$params['is_show'] : $type; //默认显示
         $this->_vars['top_delete_url'] = isset($params['url']) ? $params['url'] : $this->_vars['top_delete_url'];//默认读取 __construct 设置的url
         $this->_vars['top_delete_title'] = isset($params['title']) ? $params['title'] : '删除';//标题
         return $this;
    }

    /**
     * 定制顶部删除按钮
     * @param boolean $type  是否显示删除按钮
     * @param array $params['is_show'=>true, 'url'=>'', 'title'=>'删除']
     * @return this
     */
    public function setRightDeleteButton($params = [] ,$type = true)
    {
         $this->_vars['is_show_delete'] = isset($params['is_show']) ? (boolean)$params['is_show'] : $type; //默认显示
         $this->_vars['right_delete_url'] = isset($params['url']) ? $params['url'] : $this->_vars['right_delete_url'];//默认读取 __construct 设置的url
         $this->_vars['right_delete_title'] = isset($params['title']) ? $params['title'] : '删除';//标题
         return $this;
    }

    /**
     * qrocde|image|link 只能使用一次，因为参数问题，所以如果需要多个，请自己拓展即可（复制一个，改个名字）
     * 添加额外的按钮
     * 按钮：删除，编辑，绑定，复制按钮
     * copy_url|qrcode|image|copy_field|link:复制链接|弹出二维码|弹出图片（小程序码|其他图片链接）|复制某个字段的值，跳转链接{
     *   url:复制链接
     *   url_param_field:复制链接可以携带的参数字段 作为第一个参数
     *   url_param_extra_field:复制链接可以携带的参数字段 作为第二个参数
     *   is_hash : 是否是hash 路由
     * }//只能存在一个 并且最多只能携带两个参数
     * @param $title 按钮名称
     * @param $event 内置事件 ：
     * 目前已有：
     * image：图片（弹出窗口）
     * copy_url【复制链接】,
     * qrcode【qrcode==以二维码的方式给用户扫码(如绑定用户微信号；显示小程序的某个页面)】,
     * copy_field【复制某个字段的值】
     * link 
     */
    public function addRightButton($title = '', $event = '',  $params = []){
        
         $params['url'] = !isset($params['url'])?'':$params['url'];
         $params['title'] = !isset($params['title'])?'':$params['title'];//弹出窗口的标题（如果是弹出窗口模式：qrcode|image）
         $params['alt'] = !isset($params['alt'])?'':$params['alt'];//图片的alt image模式
         $params['url_param_field'] = !isset($params['url_param_field'])?'':$params['url_param_field'];
         $params['url_param_extra_field'] = !isset($params['url_param_extra_field'])?'':$params['url_param_extra_field'];
         $params['copy_field'] = !isset($params['copy_field'])?'':$params['copy_field'];
         //由于携带参数，所以需要验证是否是通过hash跳转==有参数时，才会需要考虑这个hash
         $params['is_hash'] = !isset($params['is_hash'])?false:$params['is_hash'];

         //按钮的类和样式
         $params['class'] = !isset($params['class'])?'layui-btn-normal':$params['class'];
         $params['style'] = !isset($params['style'])?'font-size:12px;':$params['style'];
         
         $column = [
            'title' => $title,
            'event' => $event,
            'params' => $params,
         ];
           
         $args = array_slice(func_get_args(), 6);
         $column = array_merge($column, $args);

         $this->_vars['right_buttons'][] = $column;
         return $this;
    }

    /**
     * 添加一列
     * @param string $name 字段名称
     * @param string $title 列标题
     * @param string $type 单元格类型 --
     * normal|text:常规
     * link:可以点击的链接{
     *   url:跳转链接
     *   url_param_field:跳转链接可以携带的参数字段 作为第一个参数
     *   url_param_extra_field:跳转链接可以携带的参数字段 作为第二个参数
     * }//只能存在一个 并且最多只能携带两个参数

     * edit:可以编辑 {
     *   $params[url]  更新字段url
     * 
     * }//可以存在多个
     * 
     * switch|switch1,switch2:{
     *  $params[url]  更新字段url
     *   $params['switch_text']  默认：是|否
     *   $params['on'] 默认： 1
     *   $params['off'] 默认 ：0
     *  
     * }//这个字段只能构建一次，所以复制了switch的代码创建了 switch1 和 switch2 所以一个table 最多有3个
     * rate:星级评分 //只能存在一个，需要多个请复制多一份
     * process:进度条 数据格式： 百分数，小数，分数 //只能存在一个
     * image|images:图片|多图 
     * {
     *   $params['alt'];//固定的主标题 优先级最高
     *   $params['alt_field'] //主标题 以某个字段作为主标题 优先级次于固定的主标题
     *   $title;//主标题 优先级最低
     *   $name //主图字段
     *   $params['images_field'] //数据中的多图字段 其数据以英文逗号分割 ---如果没有则读取主图字段数据
     *   $params['alts_field'] //数据中的多图对应名称字段 其数据以英文逗号分割 ---如果没有则读取主图的标题
     * }//一个table 最多一个，需要多个请复制多多份即可
     * 
     * 
     * copy:复制字段值{
     *  copy_field://需要被复制的字段名称
     * }//只能存在一个 
     * @param array $params 参数 ['sort'=>是否排序,'width'=>'','minWidth'=>'',totalRow,totalRowText,event,style,fixed,unresize,align,.........]
     * @return this
     */
    public function addColumn($name = '', $title = '', $type = '',  $params = [])
    {
         $field = $name;
         $params['align'] = !isset($params['align'])?'center':$params['align'];
         $params['url_param_field'] = !isset($params['url_param_field'])?'':$params['url_param_field'];
         $params['url_param_extra_field'] = !isset($params['url_param_extra_field'])?'':$params['url_param_extra_field'];
         //跳转链接时，由于携带参数，所以需要验证是否是通过hash跳转==有参数时，才会需要考虑这个hash
         $params['is_hash_url'] = !isset($params['is_hash_url'])?false:$params['is_hash_url'];
         $params['width'] = !isset($params['width'])?'':$params['width'];
         $params['on'] = !isset($params['on'])?1:$params['on'];
         $params['off'] = !isset($params['off'])?0:$params['off'];
         $params['switch_text'] = !isset($params['switch_text'])?'是|否':$params['switch_text'];
         $params['url'] = !isset($params['url'])?'':$params['url'];
         $params['fixed'] = !isset($params['fixed'])?'':$params['fixed'];
         $params['totalRow'] = !isset($params['totalRow'])?false:$params['totalRow'];
         $params['totalRowText'] = !isset($params['totalRowText'])?'':$params['totalRowText'];
         $params['unresize'] = !isset($params['unresize'])?false:$params['unresize'];
         $params['sort'] = !isset($params['sort'])?false:$params['sort'];
         $params['minWidth'] = !isset($params['minWidth'])?'':$params['minWidth'];

         if (!isset($params['event']) || empty($params['event'])) {
            switch($type){
                //这里可以在表格中自定义默认事件：目前未使用到
                default:
                    $params['event'] = '';
                break;    
            }
         }

         $column = [
            'name' => $name,
            'title' => $title,
            'type' => $type,
            'params' => $params,
            'field' => $field,
         ];

         $args = array_slice(func_get_args(), 6);
         $column = array_merge($column, $args);

         $this->_vars['columns'][] = $column;
         return $this;
    }

    /**
     * 一次性添加多列
     * @param array $columns 数据列
     * @return this
     */
    public function addColumns($columns = [])
    {
        if (!empty($columns)) {
            foreach ($columns as $column) {
                call_user_func_array([$this, 'addColumn'], $column);
            }
        }
        return $this;
    }

     /**
     * 返回构造构建界面的array数据
     * @access public
     * @return void
     */
    public function toArray()
    {
       $this->setSearchValue();   
       return self::$apiResult->returnArray(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }

    /**
     * 返回构造构建界面的json数据
     * @access public
     * @return void
     */
    public function toJson()
    {
       $this->setSearchValue();
       return self::$apiResult->echoJson(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }
  
 }