<?php
/*
 * @Description: 驱动器：表单提交之后 的过渡页面
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2021-04-23 10:08:25
 * @LastEditTime: 2021-07-13 11:36:05
 * @LastEditors: 阿海
 */
namespace builder\driver;
use builder\Builder;

 class Dispatch extends Builder{

   /**
     * 列间距 可选 “像素值” 数组
     * @var array
     */
    private $_space = [0, 1, 3, 5, 8, 10, 12, 15, 18, 20, 22, 28, 30];
     
    /**
     * @var array 模板参数变量
     */
    protected $_vars = [
        'store'=>'dispatch',//标识 === 作为渲染表格的标识
        'base_url'=>'',//基础页
        'is_hash'=>true,//使用hash跳转
        'location_hash'=>'',//指定 跳转的hash      hash跳转地址
        'location_url' => '',//指定 直接跳转的url  路由跳转地址
        'title'=>'操作成功',//过渡页面的提示语
        'icon_success'=>'icon-icon-test45',//表单提交成功显示的图标
        'icon_error'=>'icon-icon-test42',//表单提交失败显示的图标
        'form_result'=>true,//表单提交是否成功
        'wait_time'=>3,//过渡时，停留时间
    ];

     /**
     * 初始化
     */
    public function __construct()
    {
         parent::__construct();
         $this->_vars['base_url'] = url('cms/Index/index','',true,true);
         $this->_vars['location_url'] = $this->request->url(true);
         $this->_vars['location_hash'] = $this->request->url();
    }

    /**
     * 是否使用hash跳转，还是使用路由跳转
     * @param boolean $boolean
     * @return this
     */
    public function setIsHash($boolean = true)
    {
         $this->_vars['is_hash'] = (boolean)$boolean;
         return $this;
    }

   /**
     * 表单提交是否成功
     * @param boolean $boolean
     * @return this
     */
    public function setFormResult($boolean = true)
    {
         $this->_vars['form_result'] = (boolean)$boolean;
         return $this;
    }

    /**
     * 设置提示标题 
     */
    public function setTitle($title = '操作成功')
    {
         $this->_vars['title'] = $title;
         return $this;
    }

    /**
     * 设置停留时间
     */
    public function setWaitTime($second = 3)
    {
         $this->_vars['wait_time'] = $second;
         return $this;
    }
 
     /**
     * 指定 跳转的hash      hash跳转地址
     * @param string $title
     * @return this
     */
    public function setLocationHash($url = '')
    {
         $this->_vars['location_hash'] = $url;
         return $this;
    }

     /**
     * 指定 直接跳转的url  路由跳转地址
     * @param string $title
     * @return this
     */
    public function setLocationUrl($url = '')
    {
         $this->_vars['location_url'] = $url;
         return $this;
    }

     /**
     * 返回构造构建界面的array数据
     * @access public
     * @return void
     */
    public function toArray()
    {
        return self::$apiResult->returnArray(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }

    /**
     * 返回构造构建界面的json数据
     * @access public
     * @return void
     */
    public function toJson()
    {
        return self::$apiResult->echoJson(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }

    
 }