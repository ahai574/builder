<?php
/*
 * @Description: 驱动器：构建from表单的json数据
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2021-04-23 10:08:25
 * @LastEditTime: 2021-07-14 09:44:13
 * @LastEditors: 阿海
 */
namespace builder\driver;
use builder\Builder;
use think\Db;

 class Form extends Builder{

   /**
     * 列间距 可选 “像素值” 数组
     * @var array
     */
    private $_space = [0, 1, 3, 5, 8, 10, 12, 15, 18, 20, 22, 28, 30];
     
    /**
     * @var array 模板参数变量
     */
    protected $_vars = [
        
      'store'=>'form',//标识 === 作为渲染表格的标识
      'debug'=>false, //检测开启了那些事件，以及是否规范，打印信息 console.log
      'page_title'=>'',//页面内容区上面的标题
      'page_tips'=>'',//页面上搜索区域下面的声明文字【常用于注释给管理员】 可以使用html代码
      'is_layer_page_tips'=>false,//页面上搜索区域下面的声明文字 以什么方式显示f false:以文本方式 true:使用layer的弹出层方式显示

      'base_url'=>'',//基础页面路由
      
      'form_items' => [],// 表单项目
      '_method' => 'post', // 表单提交方式
      'post_url' => '',    // 表单提交地址
      'form_data' => [],    // 表单数据---用于重置设置的数据

      '_token_name' => '_hash_', // 表单令牌名称
      '_token_value' => '', // 表单令牌值
      'post_btn_disabled'=>false,//立即提交按钮是否可以点击
      
    ];

     /**
     * 初始化
     */
    public function __construct()
    {
         parent::__construct();
         $this->_vars['base_url'] = url('cms/Index/index','',true,true);
         $this->_vars['post_url'] = request()->url(true);
         $this->_vars['_token_value'] = request()->token($this->_vars['_token_name']);
       
    }

    /**
     * 设置表单令牌
     * @param string $name 令牌名称
     * @param string $type 令牌生成方法
     * @return $this
     */
    public function setToken($name = '__token__', $type = 'md5')
    {
        $this->_vars['_token_name'] = $name === '' ? '__token__' : $name;
        $this->_vars['_token_value'] = request()->token($this->_vars['_token_name'], $type);
        return $this;
    }

    /**
     * 提交按钮是否可以点击
     * @param string $title
     * @return this
     */
    public function setPostBtn($disabled = true)
    {
         $this->_vars['post_btn_disabled'] = $disabled;
         return $this;
    }

     /**
     * 页面内容区上面的标题
     * @param string $title
     * @return this
     */
    public function setPostUrl($url = '')
    {
         $this->_vars['post_url'] = $url;
         return $this;
    }

    /**
     * 页面内容区上面的标题
     * @param string $title
     * @return this
     */
    public function setPageTitle($title = '')
    {
         $this->_vars['page_title'] = $title;
         return $this;
    }

    /**
     * 页面上搜索区域下面的声明文字【常用于注释给管理员】 可以使用html代码
     * @param string $content
     * @return this
     */
    public function setPageTips($content = '',$isLayer = false)
    {
         $this->_vars['page_tips'] = $content;
         $this->_vars['is_layer_page_tips'] = $isLayer;
         return $this;
    } 

    /**
     * 添加单行文本框
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addText($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '') 
    {
      if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
          $array = $this->makePlaceholder($title);
          $title = $array['title'];
          $placeholder = $array['placeholder'];
      }
      $item = [
          'type' => 'text',
          'name' => $name,
          'title' => $title,
          'value' => $value,
          'tips' => $tips,
          'verify' => $verify,
          'verType' => !empty($verType) ? $verType : "msg",
          'extra_class' => $extra_class,
          'extra_attr' => $extra_attr,
          'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
          
      ];
      $this->_vars['form_items'][] = $item;
      return $this;
  }

   /**
     * 添加单行文本框
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addPassword($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '') 
    {
      if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
          $array = $this->makePlaceholder($title);
          $title = $array['title'];
          $placeholder = $array['placeholder'];
      }
      $item = [
          'type' => 'password',
          'name' => $name,
          'title' => $title,
          'value' => $value,
          'tips' => $tips,
          'verify' => $verify,
          'verType' => !empty($verType) ? $verType : "msg",
          'extra_class' => $extra_class,
          'extra_attr' => $extra_attr,
          'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
          
      ];
      $this->_vars['form_items'][] = $item;
      return $this;
  }

  /**
     * 添加多行文本框
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addTextarea($name = '',$title = '',$value = '',$tips = '',$verify = '',$verType = 'msg', $extra_class = '',$extra_attr = '') 
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'textarea',
            'name' => $name,
            'title' =>  $title,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
        ];
        $this->_vars['form_items'][] = $item;
        return $this;
    }

     /**
     * 添加hidden表单
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
   
     * @return mixed
     */
    public function addHidden($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '') 
    {
      if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
          $array = $this->makePlaceholder($title);
          $title = $array['title'];
          $placeholder = $array['placeholder'];
      }
      $item = [
          'type' => 'hidden',
          'name' => $name,
          'title' => $title,
          'value' => $value,
          'tips' => $tips,
          'verify' => $verify,
          'verType' => !empty($verType) ? $verType : "msg",
          'extra_class' => $extra_class,
          'extra_attr' => $extra_attr,
          'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
      ];
      $this->_vars['form_items'][] = $item;
      return $this;
  }

  /**
     * 添加单行文本框-- 标签框
     * @param string $name 表单项名
     * @param string $title 标题
     * @param array $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string $delimiter 分隔符号
     * @param string $prohibits 禁止被删除的标签-只允许添加{即生成的标签没有删除关闭的标签}
     * @return mixed
     */
    public function addTag($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$delimiter = ",",$prohibits="") 
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'tag',
            'name' => $name,
            'title' => $title,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
            'delimiter'=>!empty($delimiter) ? $delimiter : ",",
        ];
        $item['value'] =  is_array($value) ? implode($delimiter,$value) : $value;
        $item['prohibits'] =  is_array($prohibits) ? implode($delimiter,$prohibits) : $prohibits;
        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加任务的单行文本框
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string $url 测试运行的接口
     * @return mixed
     */
    public function addCron($name = '', $title = '', $value = '* * * * * * ',$tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $url='')
     {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'cron',
            'name' => $name,
            'title' => $title,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
            'url'=>$url
        ];
        $this->_vars['form_items'][] = $item;
        return $this;
    }

     /**
     * 添加颜色选择器
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param boolean $alpha 是否使用透明度 rgba()颜色选择器，否则使用hex 16进制颜色选择
     * @return mixed
     */
    public function addColorPicker($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg',  $extra_class = '', $extra_attr = '',$alpha = false)
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'colorPicker',
            'name' => $name,
            'alpha' => $alpha,
            'title' => $title,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
        ];
        $this->_vars['form_items'][] = $item;
        return $this;
    }

      /**
     * 添加图标
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addIcon( $name = '',$title = '', $value = 'fa fa-circle-thin',  $tips = '', $verify = '', $verType = 'msg', $extra_class = '',$extra_attr = '')
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'icon',
            'name' => $name,
            'title' => $title,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
        ];
        $this->_vars['form_items'][] = $item;
        return $this;
    }

    public function addProvince($name = 'province|city|county', $title = '',  $value = [], $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$delimiter=","){
        return $this->addAddress($name, $title, $value, $tips, $verify, $verType, $extra_class, $extra_attr, $delimiter, 'province');
    }

    public function addCity($name = 'province|city|county', $title = '',  $value = [], $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$delimiter=","){
        return $this->addAddress($name, $title, $value, $tips, $verify, $verType, $extra_class, $extra_attr, $delimiter,"city");
    }

    /**
     * 添加普通联动表单项
     * @param string $name 表单项名 请使用“|”作为分隔符,addFormItem使用的就是“|” 例如：province|city|county
     * @param string $title 表单项标题
     * @param array|string $value 默认值
     * @param string $tips 表单项提示说明
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param array $extra_attr 额外属性
     * @param string $delimiter 分隔符号
     * @param string $type "address":省市区 “province”只显示省份 “city”:显示省和市
     * @return mixed
     */
    public function addAddress($name = 'province|city|county', $title = '',  $value = [], $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$delimiter=",",$type = 'address')
    {

        $tp = $this->makePlaceholder($title);

        $item = [
            'type' => $type,
            'name' => $name,
            'title' =>  isset($tp['title']) && !empty($tp['title']) ? $tp['title'] : "",
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'delimiter'=>!empty($delimiter) ? $delimiter : ",",
            'placeholder' => isset($tp['placeholder']) && !empty($tp['placeholder']) ? $tp['placeholder'] : ["请选择"
                . $title, "请选择" . $title, "请选择" . $title, "请选择" . $title, "请选择" . $title, "请选择" . $title],
        ];

        $item['name'] =  !is_array($name) ? explode("|",$name) : $name;
        $item['value'] =  !is_array($value) ? (empty($value)?[]:explode($delimiter,$value)) : $value;

        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加复选框
     * @param string $name 复选框名
     * @param string $title 复选框标题
     * @param array $options 复选框数据
     * @param string $tips 提示
     * @param string $value 默认值 '1,2' [1,2]
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param array $extra_attr 额外属性
     * @param string $delimiter 分隔符号
     * @param string $skin 皮肤【primary|''】
     * @return mixed
     */
    public function addCheckbox($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $options = [], $delimiter = ",", $skin = "primary")
    {
        if (!is_array($value)) {
            $value = explode(",", $value);
        }
        $item = [
            'type' => 'checkbox',
            'name' => substr($name,-2) == '[]'?$name:$name."[]",
            'title' => $title,
            'options' => $options == '' ? [] : $options,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'skin' => $skin,
            'delimiter'=>!empty($delimiter) ? $delimiter : ",",
        ];
        $item['value'] =  is_array($value) ? implode($delimiter,$value) : $value;

        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加单选
     * @param string $name 单选名
     * @param string $title 单选标题
     * @param string $tips 提示
     * @param array $options 单选数据
     * @param string $value 默认值
     * @param string $verify 验证条件 暂时没有----保留
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param array $extra_attr 额外属性  ['1','2'] --->在数组中的将追加“禁用【disabled】”
     * @return mixed
     */
    public function addRadio($name = '', $title = '',  $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = [], $options = [], $skin = "primary")
    {
        $item = [
            'type' => 'radio',
            'name' => $name,
            'title' => $title,
            'options' => $options == '' ? [] : $options,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'skin' => $skin
        ];

        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加时间控件【年-月-日 时：分：秒】
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string min 最小范围内的日期时间值 时间格式【1993-01-30 12:12:12】
     * @param string max 最大范围内的日期时间值 时间格式【2050-01-30 12:12:12】
     * @param string $theme 皮肤主题 【value|grip|molv|#333333(6位的 Hex Color)】
     * @param string language 语言包【cn|en】
     * @return mixed
     */
    public function addYear( $name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $min = "1980-01-30 00:00:00", $max = "2050-01-30 00:00:00", $theme = "grid",$language = 'cn',$type='year') 
    {
        return $this->addDatetime($name , $title , $value , $tips , $verify , $verType , $extra_class , $extra_attr, $min, $max , $theme ,$language ,$type);
    }

     /**
     * 添加时间控件【年-月-日 时：分：秒】
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string min 最小范围内的日期时间值 时间格式【1993-01-30 12:12:12】
     * @param string max 最大范围内的日期时间值 时间格式【2050-01-30 12:12:12】
     * @param string $theme 皮肤主题 【value|grip|molv|#333333(6位的 Hex Color)】
     * @param string language 语言包【cn|en】
     * @return mixed
     */
    public function addMonth( $name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $min = "1980-01-30 00:00:00", $max = "2050-01-30 00:00:00", $theme = "grid",$language = 'cn',$type='month') 
    {
        return $this->addDatetime($name , $title , $value , $tips , $verify , $verType , $extra_class , $extra_attr, $min, $max , $theme ,$language ,$type);
    }

     /**
     * 添加时间控件【年-月-日 时：分：秒】
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string min 最小范围内的日期时间值 时间格式【1993-01-30 12:12:12】
     * @param string max 最大范围内的日期时间值 时间格式【2050-01-30 12:12:12】
     * @param string $theme 皮肤主题 【value|grip|molv|#333333(6位的 Hex Color)】
     * @param string language 语言包【cn|en】
     * @return mixed
     */
    public function addDate( $name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $min = "1980-01-30 00:00:00", $max = "2050-01-30 00:00:00", $theme = "grid",$language = 'cn',$type='date') 
    {
        return $this->addDatetime($name , $title , $value , $tips , $verify , $verType , $extra_class , $extra_attr, $min, $max , $theme ,$language ,$type);
    }

    /**
     * 添加时间控件【年-月-日 时：分：秒】
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string min 最小范围内的日期时间值 时间格式【1993-01-30 12:12:12】
     * @param string max 最大范围内的日期时间值 时间格式【2050-01-30 12:12:12】
     * @param string $theme 皮肤主题 【value|grip|molv|#333333(6位的 Hex Color)】
     * @param string language 语言包【cn|en】
     * @return mixed
     */
    public function addTime( $name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $min = "1980-01-30 00:00:00", $max = "2050-01-30 00:00:00", $theme = "grid",$language = 'cn',$type='time') 
    {
        return $this->addDatetime($name , $title , $value , $tips , $verify , $verType , $extra_class , $extra_attr, $min, $max , $theme ,$language ,$type);
    }

     /**
     * 添加时间控件【年-月-日 时：分：秒】
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @param string min 最小范围内的日期时间值 时间格式【1993-01-30 12:12:12】
     * @param string max 最大范围内的日期时间值 时间格式【2050-01-30 12:12:12】
     * @param string $theme 皮肤主题 【value|grip|molv|#333333(6位的 Hex Color)】
     * @param string language 语言包【cn|en】
     * @return mixed
     */
    public function addDatetime( $name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $min = "1980-01-30 00:00:00", $max = "2050-01-30 00:00:00", $theme = "grid",$language = 'cn',$type='datetime') 
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }

        $item = [
            'type' => $type,
            'name' => $name,
            'title' => $title,
            'value' => !empty($value) ? $value : date("Y", time()),
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'min' => $min,
            'max' => $max,
            'theme' => $theme,
            'language' => $language,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
        ];

        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加下拉菜单带搜索
     * @param string $name 下拉菜单名
     * @param string $title 标题
     * @param array $options 选项['key'=>'val',...] ['1'=>'男神','2'=>'女神']
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param array $extra_attr 额外属性  ['1','2'] --->在数组中的将追加“禁用【disabled】”
     * @param string $extra_class 额外css类名
     * @return mixed
     */
    public function addSelect($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $options = [],$disableds='',$type="select")
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }

        $item = [
            'type' => $type,
            'name' => $name,
            'title' => $title,
            'options' => $options,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : "请选择一项",
        ];
      
        $item['disableds'] =  !is_array($disableds) ? explode(',',$disableds) : $disableds;

        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加下拉菜单带搜索
     * @param string $name 下拉菜单名
     * @param string $title 标题
     * @param array $options 选项['key'=>'val',...] ['1'=>'男神','2'=>'女神']
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param array $extra_attr 额外属性  ['1','2'] --->在数组中的将追加“禁用【disabled】”
     * @param string $extra_class 额外css类名
     * @return mixed
     */
    public function addSelectgroup($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '', $options = [],$disableds = '',$type='selectgroup')
    {
        return $this->addSelect($name , $title , $value , $tips , $verify, $verType , $extra_class, $extra_attr, $options ,$disableds,$type);
    }

    /**
     * 添加单文件上传 使用webupload
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $value 默认值
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param int $limitNum 限制数量
     * @param string $extra_class 额外css类名
     * @param $accepts mimeTypes接受的类型
     * @return mixed
     */
    public function addFiles($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '',$limitNum=10,$type="files", $accepts='' ,$delimiter=',')
    {
        $item = [
            'type'        => $type,
            'name'        => $name,
            'title'       => $title,
            'value'       => $value,
            'tips'        => $tips,
            'verify'      => $verify,
            'verType'     => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'limit_num'   => $limitNum
        ];
        if(empty($accepts))
        $item['srcs'] = (!empty($value) && !empty($this->get_files($value,'alt'))) ? $this->get_files($value,'alt') : ['alt'=>[],'src'=>[],'thumb'=>[],'sizes'=>[]];
        $item['file_ids'] =  !is_array($value) ? explode($delimiter,$value) : $value;
        $item['accepts'] =  !empty($accepts) ? (is_array($accepts)?implode($delimiter,$accepts):$accepts) : "application/pdf,pplication/x-rar,application/zip,image/*,application/msword,application/x-zip-compressed,video/*,text/plain";
     
        $this->_vars['form_items'][] = $item;
        return $this;
    }

    /**
     * 添加单文件上传 使用webupload
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $value 默认值
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param int $limitNum 限制数量
     * @param string $extra_class 额外css类名
     * @return mixed
     */
    public function addFile($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '',$limitNum=1,$type="file",$delimiter=',')
    {
        return $this->addFiles($name , $title , $value, $tips , $verify, $verType , $extra_class,$limitNum,$type);
    }

     /**
     * 添加多张图片上传---图片上传的配置默认全部读取upload配置里面的图片配置
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addImages($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$type="images",$delimiter=',')
    {

        $item = [
            'type' => $type,
            'name' => $name,
            'title' => $title,
            'value' => $value,
            'img_ids'=>[],
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'delimiter'=>$delimiter //setFormValue 时使用
        ];
        $item['srcs'] = (!empty($value) && !empty($this->get_images($value,'alt'))) ? $this->get_images($value,'alt') : ['alt'=>[],'src'=>[],'thumb'=>[],'sizes'=>[]];
        $item['img_ids'] =  !is_array($value) ? explode($delimiter,$value) : $value;
        $this->_vars['form_items'][] = $item;
        return $this;
    }

     /**
     * 添加图片上传---图片上传的配置默认全部读取upload配置里面的图片配置
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addImage($name = '', $title = '', $value = '', $tips = '', $verify = '', $verType = 'msg', $extra_class = '', $extra_attr = '',$type="image")
    {
        return $this->addImages($name, $title , $value, $tips , $verify, $verType, $extra_class , $extra_attr ,$type);
    }

    /**
     * 添加百度编辑器ueditor
     * @param string $name 表单项名
     * @param string $title 标题
     * @param string $value 默认值
     * @param string $tips 提示
     * @param string $verify 验证条件 :required|phone|email|url|number|date|identity
     * @param string $verType tips（吸附层）alert（对话框） msg（默认）
     * @param string $extra_class 额外css类名
     * @param string $extra_attr 额外属性
     * @return mixed
     */
    public function addUeditor($name = '',$title = '', $value = '', $tips = '',$verify = '',$verType = 'msg',$extra_class = '', $extra_attr = '') 
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matches)) {
            $title = $matches[1];
            $placeholder = $matches[2];
        }
        $item = [
            'type' => 'ueditor',
            'name' => $name,
            'title' =>  $title,
            'value' => $value,
            'tips' => $tips,
            'verify' => $verify,
            'verType' => !empty($verType) ? $verType : "msg",
            'extra_class' => $extra_class,
            'extra_attr' => $extra_attr,
            'placeholder' => isset($placeholder) ? $placeholder : '请输入' . $title,
        ];
        $this->_vars['form_items'][] = $item;
        return $this;
    }

   /**
     * 添加表单项
     * @access public
     * @param string $type 表单类型
     * @param string $name 字段名
     * @return $this
     * @throws Exception
     */
    public function addFormItem($type = '', $name = '')
    {
        try {
            if (!empty($type)) {
                // 获取所有参数值
                $args = func_get_args();
                array_shift($args);
                $layout = '12|12|12|12|0|0';
                // 判断是否有布局参数
                if (strpos($type, ':')) {
                    list($type, $layout) = explode(':', $type);
                }
                if($type == 'address'){
                    $list = explode("|",$name);
                    for($i=0; $i<count($list); $i++){
                        $this->layout([$list[$i]=> $layout]);
                    }
                }
                if($type == 'checkbox'){
                    $name = substr($name,-2) == '[]'?$name:$name."[]";
                }
                $this->layout([$name=> $layout]);

                $method = 'add' . ucfirst($type);
                call_user_func_array([$this, $method], $args);
                return $this;
            } else {
                throw new Exception($type . '表单项不能为空');
            }
        } catch (Exception $e) {
            throw new Exception($type . '表单类型方法【不存在|有错误】异常');
        }
    }

     /**
     * 表单项布局
     * @access public
     * @param array $column 布局参数 ['字段名' =>xs|sm|md|lg|space|offset,......][space优先于offset,设置了space则offset不生效]
     * @return $this
     * @throws Exception
     */
    public function layout($column = [])
    {
        try {
            if (!empty($column)) {
                foreach ($column as $field => $layout) {
                    $layout = explode('|', $layout);
                    $this->_vars['layout'][$field] = 'layui-col-xs-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0) . ' layui-col-sm-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0). ' layui-col-md-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0). ' layui-col-lg-offset' . (isset($layout[5]) ? ($layout[5] == '' ? 0 : $layout[5]) : 0) . ' layui-col-space' . (isset($layout[4]) ? ($layout[4] == '' ? $this->_space[0] : $layout[4]) : $this->_space[0]) . ' layui-col-lg' . (isset($layout[3]) ? ($layout[3] == '' ? $layout[0] : $layout[3]) : $layout[0]) . ' layui-col-md' . (isset($layout[2]) ? ($layout[2] == '' ? $layout[0] : $layout[2]) : $layout[0]) . ' layui-col-sm' . (isset($layout[1]) ? ($layout[1] == '' ? $layout[0] : $layout[1]) : $layout[0]) . ' layui-col-xs' . $layout[0] ;
                }
                return $this;
            } else {
                throw new Exception('layout的参数不能为空', 9404);
            }
        } catch (Exception $e) {
            throw new Exception('layout的参数异常【("字段名"=>"xs|sm|md|lg|space|offset",......)】', 9500);
        }
    }

    /**
     * 处理placeholder
     * @param $title
     * @param $placeholder
     * @return array
     */
    protected function makePlaceholder($title, $placeholder = [])
    {
        if (preg_match('/(.*)\[:(.*)\]/', $title, $matche)) {
            $title = $matche[1];
            array_push($placeholder, $matche[2]);
            //并列多个placeholder时，
            if (preg_match('/(.*)\[:(.*)\]/', $matche[1])) {
                return $this->makePlaceholder($matche[1], $placeholder);
            }
        }
        return ['title' => $title, 'placeholder' => array_reverse($placeholder)];
    }

     /**
     * 初始化表单项的数据
     */
    public function setFormData($form_data = [])
    {
        if (!empty($form_data)) {
            $this->_vars['form_data'] = $form_data;
        }
        return $this;
    }

    /**
     * 格式化数据
     */
    private function setFormValue(){
        $data = $this->_vars['form_data'];

        foreach ($this->_vars['form_items'] as $key=>$item) {
            
            switch ($item['type']) {
                case "checkbox":
                    $name = substr($item['name'],-2) == '[]'?substr($item['name'],0,-2):$item['name'];
                    if (!empty($data) && isset($data[$name])) {
                        $this->_vars['form_items'][$key]['value'] = is_array($data[$name])?implode($item['delimiter'],$data[$name]):$data[$name];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) ? (is_array($item['value'])?implode($item['delimiter'],$item['value']):$item['value']) : '';
                    }
                    break;
                case 'year':
                    if (isset($data[$item['name']])) {
                        $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y", time());
                    }
                    //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                    if (stripos(trim($this->_vars['form_items'][$key]['value']), ":") || stripos(trim($this->_vars['form_items'][$key]['value']), "-")) {
                        $this->_vars['form_items'][$key]['value'] = date("Y", strtotime($this->_vars['form_items'][$key]['value']));
                    } else {
                        //如果大于9999那么就认为是时间戳，需要进行格式化
                        $this->_vars['form_items'][$key]['value'] = $this->_vars['form_items'][$key]['value']>9999 ? date("Y", (int) $this->_vars['form_items'][$key]['value']) : (int) $this->_vars['form_items'][$key]['value'];
                    }
                    break;
                case 'month':
                    if (isset($data[$item['name']])) {
                        $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m", time());
                    }
                    //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                    if (stripos(trim($this->_vars['form_items'][$key]['value']), ":") || stripos(trim($this->_vars['form_items'][$key]['value']), "-")) {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m", strtotime($this->_vars['form_items'][$key]['value']));
                    } else {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m", (int) $this->_vars['form_items'][$key]['value']);
                    }
                    break;
                case 'date':
                    if (isset($data[$item['name']])) {
                        $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m-d", time());
                    }
                    //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                    if (stripos(trim($this->_vars['form_items'][$key]['value']), ":") || stripos(trim($this->_vars['form_items'][$key]['value']), "-")) {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m-d", strtotime($item['value']));
                    } else {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m-d", (int) $this->_vars['form_items'][$key]['value']);
                    }
                    break;
                case 'time':
                    if (isset($data[$item['name']])) {
                        $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) && !empty($item['value']) ? $item['value'] : '00:00:00';
                    }
                    if (!preg_match("/^[0-9]{2}(:){1}[0-9]{2}(:){1}[0-9]{2}$/", $item['value']))
                        $this->_vars['form_items'][$key]['value'] = "00:00:00";
                    break;
                case 'datetime':
                    if (isset($data[$item['name']])) {
                        $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                    } else {
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) && !empty($item['value']) && $item['value'] != 0 ? $item['value'] : date("Y-m-d H:i:s", time());
                    }
                    //如果存在":"或者"-" 那么可以认为这个是有时间格式 则不用转格式
                    if (stripos(trim($this->_vars['form_items'][$key]['value']), ":") || stripos(trim($this->_vars['form_items'][$key]['value']), "-")) {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m-d H:i:s", strtotime($this->_vars['form_items'][$key]['value']));
                    } else {
                        $this->_vars['form_items'][$key]['value'] = date("Y-m-d H:i:s", (int) $this->_vars['form_items'][$key]['value']);
                    }
                    break;
                case 'image':
                case 'images':
                    try{
                        if (!empty($data) && isset($data[$item['name']])) {
                            $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                        } else {
                            $this->_vars['form_items'][$key]['value'] = isset($item['value']) ? $item['value'] : '';
                        }
                    }catch(\Exception $e){
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) ? $item['value'] : '';
                    }
                    $this->_vars['form_items'][$key]['srcs'] = (!empty($this->_vars['form_items'][$key]['value']) && !empty(get_images($this->_vars['form_items'][$key]['value'],'alt'))) ? get_images($this->_vars['form_items'][$key]['value'],'alt') : ['alt'=>[],'src'=>[],'thumb'=>[],'sizes'=>[]];
                    $this->_vars['form_items'][$key]['img_ids'] =  !is_array($this->_vars['form_items'][$key]['value']) ? explode($this->_vars['form_items'][$key]['delimiter'],$this->_vars['form_items'][$key]['value']) : $this->_vars['form_items'][$key]['value'];
                    break;    
                default:
                
                    try{
                        if (!empty($data) && isset($data[$item['name']])) {
                            $this->_vars['form_items'][$key]['value'] = $data[$item['name']];
                        } else {
                            $this->_vars['form_items'][$key]['value'] = isset($item['value']) ? $item['value'] : '';
                        }
                    }catch(\Exception $e){
                        $this->_vars['form_items'][$key]['value'] = isset($item['value']) ? $item['value'] : '';
                    }
            }
        }
    }

     /**
     * 返回构造构建界面的array数据
     * @access public
     * @return void
     */
    public function toArray()
    {
        $this->setFormValue();
        return self::$apiResult->returnArray(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }

    /**
     * 返回构造构建界面的json数据
     * @access public
     * @return void
     */
    public function toJson()
    {
        $this->setFormValue();
        return self::$apiResult->echoJson(0,['msg'=>'请求成功','data'=>$this->_vars]);
    }

    
    /**
     * 从附件里面读取图片文件的url
     * @param int $id 附件库的主键id
     * @param string $field [path|thumb|url|all]
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function get_file_path($id = 0, $field = 'path')
    {
        $root_url = $this->root_url();
        if ($id == 0) {
            return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/images/no-image.png";
        } else {
            @$find = Db::name("attachment")->where('id', (int) $id)->findOrEmpty();
            if (!empty($find)) {
                switch ($field) {
                    case "all":
                        return $find;
                        break;
                    case "thumb":
                        return  "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find['thumb'];
                        // return "/" . $find['thumb'];
                        break;
                    case "url":
                        return  "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find['url'];
                        // return  "/" . $find['url'];
                        break;
                    default:
                        return "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find['path'];
                        // return  "/" . $find['path'];
                        break;
                }
            } else {
                return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/images/no-image.png";
            }
        }
    }

    /**
     * 从附件里面读取图片文件的url
     * @param int $ids 附件库的主键组合 以英文逗号隔开
     * @param string $field [path|thumb|url|all]
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function get_files($ids = 0, $field = 'path')
    {
        $root_url = $this->root_url();
        if ($ids == 0) {
            return [];
        } else {
        
            if (!empty($ids)) {
                @$find = Db::name("filelist")->where([['id','in',$ids]])->column('path,url,thumb,name,size','id');
                $ids = explode(",",$ids);
                //排序
                switch ($field) {
                    case "all":
                        return $find;
                        break;
                    case "thumb":
                        for($i=0;$i<count($find);$i++){
                            $arr[] = "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find[$ids[$i]]['thumb'];
                        }
                        break;
                    case "url":
                        for($i=0;$i<count($find);$i++){
                            $arr[] = "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find[$ids[$i]]['url'];
                        }         
                        break;
                    case "alt":
                        for($i=0;$i<count($ids);$i++){
                            $arr['alt'][] =  isset($find[$ids[$i]]['name'])?$find[$ids[$i]]['name']:'<span class="layui-bg-red">文件库已删除</span>';
                            $arr['sizes'][] =  isset($find[$ids[$i]]['size'])?$find[$ids[$i]]['size']:0;
                            $arr['thumb'][] = isset($find[$ids[$i]]['thumb'])?("//" . $_SERVER['HTTP_HOST'] . $root_url . "/" .$find[$ids[$i]]['thumb']):'' ;
                            $arr['src'][] = isset($find[$ids[$i]]['url'])?(!empty($find[$ids[$i]]['url']) ? $find[$ids[$i]]['url'] : ("//" . $_SERVER['HTTP_HOST'] . $root_url . "/" .$find[$ids[$i]]['path'])):'';
                        }         
                        break;    
                    default:
                        for($i=0;$i<count($find);$i++){
                            $arr[] = "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find[$ids[$i]]['path'];
                        }
                        break;
                }

                
                return $arr;

            } else {

                return [];
            }
        }
    }

    //eg:alpha_tp_5.1/index.php || alpha_tp_5.1/public/index.php
    //返回路由入口之前的路径 
    private function root_url(){
        $root_url = substr(request()->root(), 0, strrpos(request()->root(), '/') + 1);
        if (substr($root_url, -7) == 'public/') {
            $root_url = substr($root_url, 0, -8);
        }
        if (substr($root_url, -1) == '/') {
            $root_url = substr($root_url, 0, -1);
        }
        return $root_url;
    }

    /**
     * 从附件里面读取图片文件的url
     * @param int $id 附件库的主键id
     * @param string $field [path|thumb|url|all]
     * @param int $default 默认图片
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function get_image($id = 0, $field = 'path', $default=0)
    {
        $root_url = $this->root_url();
        if ($id == 0) {
            switch ($default) {
                case 0:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/no-pic.jpg";
                break;
                case 1:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/no-image.png";
                break;
                case 2:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/no-headimg.jpg";
                break; 
                case 3:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/banner-votes.jpg";
                break; 
                case 4:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/banner-opus.jpg";
                break;  
                default:
                    return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/no-pic.jpg";
                break;
            }
            
            
        } else {
            @$find = Db::name("filelist")->where('id', (int) $id)->findOrEmpty();
            if (!empty($find)) {
                switch ($field) {
                    case "all":
                        return $find;
                        break;
                    case "thumb":
                        return  "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find['thumb'];
                        // return "/" . $find['thumb'];
                        break;
                    case "url": //远程图片
                        return  $find['url'];
                        // return  "/" . $find['url'];
                        break;  
                    case "base64":
                        return  $find['base64'];   
                        break;  
                    default:
                        return "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find['path'];
                        // return  "/" . $find['path'];
                        break;
                }
            } else {
                return "//" . $_SERVER['HTTP_HOST'] . config("template.tpl_replace_string.__STATIC__") . "/builder/image/no-image.png";
            }
        }
    }

        /**
         * 从附件里面读取图片文件的url
         * @param int $ids 附件库的主键组合 以英文逗号隔开
         * @param string $field [path|thumb|url|all]
         * @return mixed
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         */
    private function get_images($ids = 0, $field = 'path')
    {
        $root_url = $this->root_url();
        if ($ids == 0) {
            return [];
        } else {
        
            if (!empty($ids)) {
                @$find = Db::name("filelist")->where([['id','in',$ids]])->column('path,url,thumb,name,size','id');
                //排序
                $ids = explode(",",$ids);
            
                switch ($field) {
                    case "all":
                        return $find;
                        break;
                    case "thumb":
                        for($i=0;$i<count($find);$i++){
                            $arr[] = "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find[$ids[$i]]['thumb'];
                        }
                        break;
                    case "url":
                        for($i=0;$i<count($find);$i++){
                            $arr[] = $find[$ids[$i]]['url'];
                        }         
                        break;
                    case "base64":
                        for($i=0;$i<count($find);$i++){
                            $arr[] = $find[$ids[$i]]['base64'];
                        } 
                        break;
                    case "alt":
                        for($i=0;$i<count($ids);$i++){
                            $arr['alt'][] =  isset($find[$ids[$i]]['name'])?$find[$ids[$i]]['name']:'<span class="layui-bg-red">文件库已删除</span>';
                            $arr['sizes'][] =  isset($find[$ids[$i]]['size'])?$find[$ids[$i]]['size']:0;
                            $arr['thumb'][] = (isset($find[$ids[$i]]['thumb']) && !empty($find[$ids[$i]]['thumb']))?("//" . $_SERVER['HTTP_HOST'] . $root_url . "/" .$find[$ids[$i]]['thumb']):(isset($find[$ids[$i]]['url'])?(!empty($find[$ids[$i]]['url']) ? $find[$ids[$i]]['url'] : ("//" . $_SERVER['HTTP_HOST'] . $root_url . "/" .$find[$ids[$i]]['path'])):'');
                            $arr['src'][] = isset($find[$ids[$i]]['url'])?(!empty($find[$ids[$i]]['url']) ? $find[$ids[$i]]['url'] : ("//" . $_SERVER['HTTP_HOST'] . $root_url . "/" .$find[$ids[$i]]['path'])):'';
                        
                        }       
                        break;    
                    default:
                        for($i=0;$i<count($find);$i++){
                            $arr[] = "//" . $_SERVER['HTTP_HOST'] . $root_url . "/" . $find[$ids[$i]]['path'];
                        }
                        break;
                }

                
                return $arr;

            } else {
                return [];
            }
        }
    }

 }
