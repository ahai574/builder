<?php
/*
 * @Description: 
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2020-12-14 15:36:23
 * @LastEditTime: 2021-06-16 14:00:00
 * @LastEditors: 阿海
 */

$baseUrl = request()->rootUrl()."/public";
return [
    // 模板引擎类型 支持 php think 支持扩展
    'type'         => 'Think',
    // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写 3 保持操作方法
    'auto_rule'    => 1,
    // 模板路径
    'view_path'    => 'template'.DIRECTORY_SEPARATOR,
    // 模板后缀
    'view_suffix'  => 'html',
    // 模板文件名分隔符
    'view_depr'    => DIRECTORY_SEPARATOR,
    // 模板引擎普通标签开始标记
    'tpl_begin'    => '{',
    // 模板引擎普通标签结束标记
    'tpl_end'      => '}',
    // 标签库标签开始标记
    'taglib_begin' => '{',
    // 标签库标签结束标记
    'taglib_end'   => '}',
    //模板文件输出的内容进行字符替换
    'tpl_replace_string'     =>  [
        '__STATIC__'         =>    $baseUrl.'/static',
        '__BUILDER__'        =>    $baseUrl.'/static/builder',
        '__BUILDER_JS__'     =>    $baseUrl.'/static/builder/js',
        '__BUILDER_CSS__'    =>    $baseUrl.'/static/builder/css',
        '__BUILDER_IMG__'    =>    $baseUrl.'/static/builder/image',
        '__JS__'             =>    $baseUrl.'/static/js',
        '__CSS__'            =>    $baseUrl.'/static/css',
        '__IMG__'            =>    $baseUrl.'/static/image',
        '__UPLOAD__'         =>    $baseUrl.'/runtime/uploads',
        '__ROOT__'           =>    $baseUrl,
        '__LAYER__'          =>    $baseUrl.'/static/builder/layui',
        '__LAYER_MODULES__'  =>    $baseUrl.'/static/builder/layuiModules',
        '__LAYER_CSS__'      =>    $baseUrl.'/static/builder/layui/css',
        '__LAYER_FONT__'     =>    $baseUrl.'/static/builder/layui/font',
        '__VERSION__'        =>    '1.1.1'
    ]
];