
function randomNum(minNum, maxNum,type) {
    switch (arguments.length) {
        case 1:
            break;
        case 2:
             // 函数返回 min（包含）～ max（不包含）之间的数字：整数
            return Math.floor(Math.random() * (maxNum - minNum) ) + minNum;
            break;
        case 3:
            if(type=='include'){
                // 函数返回 min（包含）～ max（包含）之间的数字：整数
                return Math.floor(Math.random() * (maxNum - minNum + 1) ) + minNum;
            }
            if(type=='random'){
                // 函数返回 min（包含）～ max（不包含）之间的数字：随机数
                return Math.random() * (maxNum - minNum) + minNum;
            }
            // 函数返回 min（包含）～ max（不包含）之间的数字：整数
            return Math.floor(Math.random() * (maxNum - minNum) ) + minNum;
            break;
        default:
            
            break;    
    }
}


//是否是数组
function isArray(arr){
    if(Array.prototype.isPrototypeOf(arr)&&arr.length >= 0){return true;}
    return fasle
}
//是否是空数组
function isEmptyArray(arr){
    if(Array.prototype.isPrototypeOf(arr)&&arr.length === 0){return true;}
    return false;
}
//是否是空对象
function isEmptyObject(obj){
    if(Object.prototype.isPrototypeOf(obj)&&Object.keys(obj).length === 0){return true;}
    return false;
}
//对象转一维数组
function objToArray(array) {
    var arr = []
    for (var i in array) {
        arr.push(array[i]);
    }
    // console.log(arr);
    return arr;
}

//对象转二维数组
function objToTwoDimensionArray(array) {
    const objArray = []

    for (let key in array) {
        objArray[key] = array[key];
    }

    return objArray;

}


//判断是否在数组中
function in_array(str, arr) {
    if(str == undefined || str == '' || arr == undefined){
        return false;
    }

    for (s = 0; s < arr.length; s++) {
        var thisEntry = arr[s].toString();
        if (thisEntry == str) {
            return true;
        }
    }
    return false;
}

//去除数组中非法值
function trimSpace(array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == "" || array[i] == null || typeof (array[i]) == "undefined") {
            array.splice(i, 1);
            i = i - 1;

        }
    }
    return array;
}

//删除数组中指定的值
function array_splice(array, value) {
    array.splice(array_search(array, value), 1);
    return array;
}

//指定删除数组中的值的下标
function array_search(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return i;
        }
    }
    return false;
}

/**
 * [获取URL中的参数名及参数值的集合]
 * 示例URL:http://htmlJsTest/getrequest.html?uid=admin&rid=1&fid=2&name=小明
 * @param {[string]} urlStr [当该参数不为空的时候，则解析该url中的参数集合]
 * @return {[string]}       [参数集合]
 */
function get_url_params(urlStr) {
    if (typeof urlStr == "undefined") {
        var url = decodeURI(location.search); //获取url中"?"符后的字符串
    } else {
        var url = "?" + urlStr.split("?")[1];
    }
    var array = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            array[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
        }
    }
    return array;
}

/**
 * [通过参数名获取url中的参数值]
 * 示例URL:http://htmlJsTest/getrequest.html?uid=admin&rid=1&fid=2&name=小明
 * @param  {[string]} paramName [参数名]
 * @return {[string]}           [参数值]
 */
function get_url_param_value(paramName) {
    var query = decodeURI(window.location.search.substring(1));
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == paramName) { return pair[1]; }
    }
    return null;
}

/**
 * @function escape_text 转义html脚本 < > & " ' : 
 * @param a -
 *            字符串
 */
function escape_text(a){
    a = "" + a;
    return a.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&apos;").replace(/:/g, "%3A").replace(/ /g, "+");
}

/**
 * @function unescape_text 还原html脚本 < > & " '
 * @param a -
 *            字符串
 */
function unescape_text(a){
    a = "" + a;
    return a.replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&apos;/g, "'").replace(/%3A/g, ":").replace(/\+/g, " ");
}

//将秒数转换为时分秒格式
function formatSeconds(value) {

    var theTime = parseInt(value);// 秒
    var middle = 0;// 分
    var hour = 0;// 小时

    if (theTime > 60) {
        middle = parseInt(theTime / 60);
        theTime = parseInt(theTime % 60);
        if (middle > 60) {
            hour = parseInt(middle / 60);
            middle = parseInt(middle % 60);
        }
    }
    var result = "" + parseInt(theTime) + "秒";
    if (middle > 0) {
        result = "" + parseInt(middle) + "分" + result;
    }
    if (hour > 0) {
        result = "" + parseInt(hour) + "小时" + result;
    }
    return result;
}

//cookie Util

var cookieUtil = {

    //添加cookie

    setCookie: function (name, value, expires) {

        var cookieText = encodeURIComponent(name) + "=" +

            encodeURIComponent(value);

        if (expires && expires instanceof Date) {

            cookieText += ";path=/; expires=" + expires;

        }

        // if (domain) {

        //   cookieText += "; domain=" + domain;

        // }

        document.cookie = cookieText;
        // console.log(cookieText);
        // console.log(document.cookie);

    },

    //获取cookie

    getCookie: function (name) {

        var cookieText = decodeURIComponent(document.cookie);

        var cookieArr = cookieText.split("; ");

        for (var i = 0; i < cookieArr.length; i++) {

            var arr = cookieArr[i].split("=");

            if (arr[0] == name) {

                return arr[1];

            }

        }

        return null;

    },

    //删除cookie

    unsetCookie: function (name) {

        document.cookie = encodeURIComponent(name) + "=; path=/;expires=" +

            new Date(0);

    }

};

