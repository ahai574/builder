/*
 * @Description: 
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2020-12-29 10:51:13
 * @LastEditTime: 2021-05-11 16:29:17
 * @LastEditors: 阿海
 */
//构造函数
function Router() {
    this.currentUrl = '';
    this.handlers = {};
    this.init = function(){
        this.refresh = this.refresh.bind(this);
        this.addStateListener();
        window.addEventListener('load', this.refresh, false);
        window.addEventListener('popstate', this.refresh, false);
        window.addEventListener('pushState', this.refresh, false);
        window.addEventListener('replaceState', this.refresh, false);
    }
    this.addStateListener = function() {
        const listener = function (type) {
            var orig = history[type];
            return function () {
                var rv = orig.apply(this, arguments);
                var e = new Event(type);
                e.arguments = arguments;
                window.dispatchEvent(e);
                return rv;
            };
        };
        window.history.pushState = listener('pushState');
        window.history.replaceState = listener('replaceState');
    }
    this.refresh = function(event) {
        this.currentUrl = location.pathname;
        this.emit('change', location.pathname);
    }
    this.on = function(evName, listener) {
        this.handlers[evName] = listener;
    }
    this.emit = function(evName, ...args) {
        const handler = this.handlers[evName];
        if (handler) {
            handler(...args);
        }
    }
    this.init();
    return this;
}
