<?php
/*
 * @Description: 
 * @ComposerRequire: 
 * @ComposerRemove: 
 * @Author: 阿海 <764882431@qq.com>
 * @Date: 2021-03-24 16:35:22
 * @LastEditTime: 2021-07-14 09:50:12
 * @LastEditors: 阿海
 */


namespace app\demo\controller;
use think\Controller;

class Index extends Controller
{

    protected $middleware = [
        // 'CheckLogin',  
        'CheckToken' => ['only'   => ['index2','add','getPage'] ]
    ];

    public function index()
    {
        // app("builder")->
        // app('php_session')->delete('auth_data');
        // dump(  app('php_session')->get('auth_data'));
        return $this->fetch('builder/layout');
    }

   

    public function index2()
    {
        $pageTips = '构造<font style="color:black">table</font>页面，页面参数，获取json数据， 渲染 （json数据包含：请求url,字段，图片，多图，状态，可编辑，删除，编辑url,绑定按钮，其他按钮（可扩展），渲染表格）<br/> 构造<font style="color:black">form</font>页面，页面参数，获取json数据，渲染（json数据包含，保存接口，渲染表单）';
        $search = [['title:12|12|6|6','标题[:搜索标题]','text'],['454s:12|12|6|6','起始时间','year','2021-05-12 00:00:00'],['credate_time:12|12|6|6','起始时间','month','2021-05-12 00:00:00'],['crfeate_time:12|12|6|6','起始时间','time','2021-05-12 00:00:00'],['create_fstime:12|12|6|6','起始时间','datetime','2021-05-12 00:00:00'],['last_timae:12|12|6|6','起始时间','date','2021-05-12'],['sort:12|12|6|6','分类[:查找分类]','select','b',[['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']]]];
        return app("builder")
                ->store('table')
                ->setTotalRow()
                ->setTopAddButton(['title'=>'添加用户','is_show'=>true,'url'=>url('demo/index/add','')],'')
                ->setPageTips($pageTips,false)
                ->setRowDrag(url('index/index/index'),true)
                ->setTopDeleteButton(['url'=>url('delete'),'title'=>'删除操作'])
                ->setRightColumn('操作')
                ->addColumn('type','TYPE','edit',['event'=>'ids_event','style'=>'color:red;font-size:18px;','width'=>'20%','totalRow'=>true,'fixed'=>'left'])
                ->addColumn('id','ID','normal',['event'=>'id_event','style'=>'color:red;font-size:18px;','width'=>100,'totalRow'=>true,'sort'=>true,'fixed'=>'left'])
                ->addColumn('id','ID','normal',['event'=>'id_event','style'=>'color:red;font-size:18px;','width'=>100,'totalRow'=>true,'sort'=>true])
                ->addColumn('id','ID','normal',['event'=>'id_event','style'=>'color:red;font-size:18px;','width'=>100,'totalRow'=>true,'sort'=>true])
                ->addColumn('title','Title','edit',['event'=>'','style'=>'color:red;font-size:18px;','totalRowText'=>'合计','width'=>"20%"])
                ->addColumn('title','跳转url','link',['url'=>url("demo/index/index2"),'url_param_field'=>'id','url_param_extra_field'=>'type','style'=>'color:red;font-size:18px;','totalRowText'=>'合计','width'=>"20%",'is_hash'=>true])
                ->addColumn('type','Trpe','switch',['width'=>100,'event'=>'','style'=>'color:blue;font-size:18px;','totalRowText'=>'合计','url'=>'index2'])
                 ->addColumn('img_src','图片','image',['width'=>150,'alts_field'=>'alts','images_field'=>'img_srcs'])
                  ->addColumn('rate','rate','rate',['width'=>150])
                 ->addColumn('id','Id','switch1',['width'=>100,'url'=>'index'])
                ->addColumn('process','process','process',['width'=>100,'url'=>'index'])
                ->setSearch($search,url('demo/index/getPage'))
                ->addRightButton('复制链接','copy_url',['url'=>url('index2'),'url_param_field'=>'id','url_param_extra_field'=>'title','is_hash'=>true])
                ->addRightButton('复制字段','copy_field',['copy_field'=>'title'])
                ->addRightButton('二维码','qrcode',['url'=>url('index3'),'url_param_field'=>'type','url_param_extra_field'=>'id','is_hash'=>false])
                ->addRightButton('跳转','link',['url'=>url('demo/index/homePage'),'url_param_field'=>'id','url_param_extra_field'=>'title','is_hash'=>true])

                ->setDataUrl(url('demo/index/getPage'))
                ->toJson();
    }

    public function postFormUrl(){
        $data = ['form_data'=>input('post.')];
        if(rand(1,8)>5){
            $hash = url('demo/index/index2');
            return app("builder")->store("dispatch")->setIsHash(true)->setLocationHash($hash)->toJson();
        }
        $url = url('demo/Index/index','',true,true)."#".input('get.hash');

        return app("builder")->store("dispatch")->setIsHash(false)->setFormResult(false)->setTitle("操作失败")->setLocationUrl($url)->toJson();
    }



    public function add(){
        $group = [
            [ 
                'title'=>'组1','options'=>
                [
                    ['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']
                ] 
            ],
            [ 
                'title'=>'组2','options'=>
                [
                    ['title'=>'选项3','value'=>'3']
                ] 
            ]
        ];
        $options = [
            ['value'=>1,'title'=>'选项1'],
            ['value'=>3,'title'=>'选项3'],
            ['value'=>11,'title'=>'选项11'],
            ['value'=>2,'title'=>'选项2']
        ];
         $pageTips = '构造<font style="color:black">table</font>页面，页面参数，获取json数据， 渲染 （json数据包含：请求url,字段，图片，多图，状态，可编辑，删除，编辑url,绑定按钮，其他按钮（可扩展），渲染表格）<br/> 构造<font style="color:black">form</font>页面，页面参数，获取json数据，渲染（json数据包含，保存接口，渲染表单）';
        // $search = [['title:12|12|6|6','标题[:搜索标题]','text'],['454s:12|12|6|6','起始时间','year','2021-05-12 00:00:00'],['credate_time:12|12|6|6','起始时间','month','2021-05-12 00:00:00'],['crfeate_time:12|12|6|6','起始时间','time','2021-05-12 00:00:00'],['create_fstime:12|12|6|6','起始时间','datetime','2021-05-12 00:00:00'],['last_timae:12|12|6|6','起始时间','date','2021-05-12'],['sort:12|12|6|6','分类[:查找分类]','select','b',[['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']]]];
        return app("builder")
                ->store('form')
                ->setPageTitle("我是标题")
                // ->setPageTips($pageTips,false)
                ->setPostUrl(url('demo/index/postFormUrl'))
                ->addFormItem('text:7','title','标题','764882431@qq.com','提示词','required','tips')
                // ->addFormItem('hidden','id','标题',input('get.id'))

                ->addFormItem('password:7','pwd','密码','我是密码','提示词','required','msg')
                ->addFormItem('tag:8','tags','标签','afds,agag,574,ahai,455|as','提示词2222额','required','msg',"","","|","574")
                ->addFormItem('tag:9','news_tags','标签2',['aa','dfs'],'提示词2222额','required','msg',"","",",","ahai,阿海")
                ->addFormItem("ueditor",'content','编辑器','算法')
                ->addFormItem("image",'img_id','单图','1','','required')
                ->addFormItem("images",'img_ids','多2图','1,2,3')
                ->addFormItem("files",'file_ids','多文件','2','','required')
                ->addFormItem("file",'file_id','单文件','4')
                ->addFormItem("year",'year','年','2021')
                ->addFormItem("month",'month','月','2021-05')
                ->addFormItem("date",'date','日','2021-05-31')
                ->addFormItem("time",'time','时间','00:23:00')
                ->addFormItem("datetime",'datetime','日期','2021-05-21 10:51:55')
                ->addFormItem('selectgroup:12','categroy','分组',3,'','','','','',$group)
                ->addFormItem('select:12','sort_category','分类',2,'','','','','',$options)
                ->addFormItem('address:12' , 'province|city|county','地址',["广东省",'广州市'])
                ->addFormItem('checkbox:12','check_box','多选','1,11','','','','','',$options,",","primary")
                ->addFormItem('radio:12','radio_box_2','单选','3','','','','','',$options,"primary")
                ->addFormItem('textarea:12','textarea','文本域','我的文本内容','提示词2222','required','msg')
              
                ->addFormItem('cron:6','cron','任务','* * * * ','sad','','','','',url('demo/index/cron'))
                ->addFormItem('colorPicker:6','color_picker','颜色选择','blue','afasf','','','','',true)
                ->addFormItem('icon:12','icon','图标')
                ->toJson();
    }

    public function cron(){
       $data = [
           '0,33 * * * * ?',
           '* * * * * ?'
       ];
       return app("api_result")->echoJson(0,['msg'=>'请求成功','data'=>$data]);
    }

    public function getPage(){
        $list = db("china")->append(['process','rate','img_srcs','img_src','alt','alts'])
                ->withAttr('img_srcs',function($value){
                      return "https://bpic.588ku.com/element_banner/20/21/05/b6f0f1fd27de9984e51383304515a9a2.png,https://bpic.588ku.com/element_banner/20/21/05/a5e978829c23be396b2694f736d93325.jpg,http://new.mainweb.com.cn/runtime/uploads/images/original/2021-06-01/60b5f76c916dc.jpg";
                })
                ->withAttr('img_src',function($value){
                    return rand(1,10)>5?"https://bpic.588ku.com/element_banner/20/21/05/a5e978829c23be396b2694f736d93325.jpg":"https://bpic.588ku.com/element_banner/20/21/05/b6f0f1fd27de9984e51383304515a9a2.png";
                })
                ->withAttr('alts',function($value){
                      return "我是001,我是002,我是003";
                })
                ->withAttr('alt',function($value){
                    return "我是001";
                })
                ->withAttr('process',function($value){
                    return rand(1,5)/5;
                })
                ->withAttr('rate',function($value){
                    return rand(1,5)+(rand(0,1)?0.5:0);
                })
                ->paginate(input('pageSize',10));
        return app("api_result")->echoJson(0,['msg'=>'请求成功','data'=>$list,'get'=>input('get.')]);
    }

    public function delete(){
         return app("api_result")->echoJson(0,['msg'=>'请求成功','data'=>input('post.')]);
    }

    public function edit(){
        $group = [
            [ 
                'title'=>'组1','options'=>
                [
                    ['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']
                ] 
            ],
            [ 
                'title'=>'组2','options'=>
                [
                    ['title'=>'选项3','value'=>'3']
                ] 
            ]
        ];
        $options = [
            ['value'=>1,'title'=>'选项1'],
            ['value'=>3,'title'=>'选项3'],
            ['value'=>11,'title'=>'选项11'],
            ['value'=>2,'title'=>'选项2']
        ];
         $pageTips = '构造<font style="color:black">table</font>页面，页面参数，获取json数据， 渲染 （json数据包含：请求url,字段，图片，多图，状态，可编辑，删除，编辑url,绑定按钮，其他按钮（可扩展），渲染表格）<br/> 构造<font style="color:black">form</font>页面，页面参数，获取json数据，渲染（json数据包含，保存接口，渲染表单）';
        // $search = [['title:12|12|6|6','标题[:搜索标题]','text'],['454s:12|12|6|6','起始时间','year','2021-05-12 00:00:00'],['credate_time:12|12|6|6','起始时间','month','2021-05-12 00:00:00'],['crfeate_time:12|12|6|6','起始时间','time','2021-05-12 00:00:00'],['create_fstime:12|12|6|6','起始时间','datetime','2021-05-12 00:00:00'],['last_timae:12|12|6|6','起始时间','date','2021-05-12'],['sort:12|12|6|6','分类[:查找分类]','select','b',[['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']]]];
        $data = db("china")->where('id',input('get.id'))->withAttr("create_time",function($value){
            return time();
        })->find();
        return app("builder")
                ->store('form')
                ->setPageTitle("我是标题")
                ->setFormData($data)
                // ->setPageTips($pageTips,false)
                ->setPostUrl(url('demo/index/postFormUrl'))
                ->addFormItem('text:7','title','标题','764882431@qq.com','提示词','required','tips')
                ->addFormItem('hidden','id','标题',input('get.id'))

                ->addFormItem('password:7','pwd','密码','我是密码','提示词','required','msg')
                ->addFormItem('tag:8','tags','标签','afds,agag,574,ahai,455|as','提示词2222额','required','msg',"","","|","574")
                ->addFormItem('tag:9','news_tags','标签2',['aa','dfs'],'提示词2222额','required','msg',"","",",","ahai,阿海")
                ->addFormItem("ueditor",'content','编辑器','算法')
                ->addFormItem("image",'img_id','单图','1','','required')
                ->addFormItem("images",'img_ids','多2图','1,2,3')

                ->addFormItem("files",'file_ids','多文件','2','','required')
                
                ->addFormItem("file",'file_id','单文件','4')
            
              
                ->addFormItem("year",'year','年','2021')
                ->addFormItem("month",'month','月','2021-05')
                ->addFormItem("date",'update_time','日','2021-05-31')
                ->addFormItem("time",'time','时间','00:23:00')
                ->addFormItem("datetime",'create_time','日期','2021-05-21 10:51:55')
                ->addFormItem('selectgroup:12','categroy','分组',3,'','','','','',$group)
                ->addFormItem('select:12','sort_category','分类',2,'','','','','',$options)
                ->addFormItem('address:12' , 'province|city|county','地址',["广东省",'广州市'])
                ->addFormItem('checkbox:12','check_box','多选','1,11','','','','','',$options,",","primary")
                ->addFormItem('radio:12','radio_box','单选','3','','','','','',$options,",","primary")
                
                ->addFormItem('textarea:12','textarea','文本域','我的文本内容','提示词2222','required','msg')
              
                ->addFormItem('cron:6','cron','任务','* * * * ','sad','','','','',url('demo/index/cron'))
                ->addFormItem('colorPicker:6','color_picker','颜色选择','blue','afasf','','','','',true)
                ->addFormItem('icon:12','icon','图标')
                ->toJson();
    }
    
    public function homePage()
    {
        $group = [
                    [ 
                        'title'=>'组1','options'=>
                        [
                            ['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']
                        ] 
                    ],
                    [ 
                        'title'=>'组2','options'=>
                        [
                            ['title'=>'选项3','value'=>'3']
                        ] 
                    ]
                ];
        $search = [
                    ['hid:12|12|4|4','标题[:搜索标题]','hidden','45'],
                    ['title:12|12|4|4','标题[:搜索标题]','text'],
                    ['aaaee:12|12|4|4','分类[:查找分类]','selectgroup','3',$group],
                    ['create_time:12|12|4|4','起始时间[:请选择时间]','datetime',''],
                    ['sort:12|12|4|4','分类[:查找分类]','select','默认值',[['title'=>'选项1','value'=>'a'],['title'=>'选项2','value'=>'b']]]
                ];
        return app("builder")
                ->store('table')
                ->setRowDrag()
                ->setDebug(false)
                ->setPageTitle("你好")
                ->setTopAddButton(['title'=>'添加用户','is_show'=>true,'url'=>url('demo/index/index','',true,true)."#homepage"],'qrcode')
                ->setDataUrl(url('demo/index/getPage'))
                ->setTopDeleteButton(['url'=>url('delete'),'title'=>'删除操作','type'=>'qrcode'])
                ->addColumn('id','ID','normal')
                ->addColumn('ids','ID','normal')
                ->setSearch($search,url('index/index/index2'))
                ->toJson();
    }

}